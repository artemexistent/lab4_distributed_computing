package c;

import lombok.SneakyThrows;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class AddAndRemoveTown implements Runnable{
  Random random = new Random();

  @SneakyThrows
  @Override
  public void run() {
    while (true) {
      int x = random.nextInt(Main.map.graph.size());

      for (int i = 0; i < Main.map.graph.get(x).size(); i ++) {
        Main.map.remove(x, i);
      }

      Main.map.graph.add(Collections.synchronizedList(new ArrayList<>()));

      Thread.sleep(20000);
    }
  }
}
