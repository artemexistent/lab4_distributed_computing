package c;

import lombok.SneakyThrows;

import java.util.Random;

public class AddAndRemoveCost implements Runnable {
  private Random random = new Random();

  @SneakyThrows
  @Override
  public void run() {
    while (true) {
      int x = Main.map.graph.size();
      int x1 = random.nextInt(x);
      int y = Main.map.graph.get(x1).size();
      int y1 = random.nextInt(y);

      Main.map.remove(x1, y1);

      Main.map.add(x1, random.nextInt(x1), random.nextInt());

      Thread.sleep(10000);
    }
  }
}
