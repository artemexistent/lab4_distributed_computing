package c;

import lombok.SneakyThrows;

import java.util.Random;

public class ChangeCosts implements Runnable{

  Random random = new Random();

  @SneakyThrows
  @Override
  public void run() {
    while (true) {
      int x = random.nextInt(Main.map.graph.size());
      int y = random.nextInt(Main.map.graph.get(x).size());

      Main.map.graph.get(x).get(y).second = random.nextInt();
      System.out.println(Main.map.toString());
      Thread.sleep(10000);
    }
  }
}
