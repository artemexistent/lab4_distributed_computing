package c;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Map {

  public List<List<Pair<Integer, Integer>>> graph = Collections.synchronizedList(new ArrayList<>());

  public Map() {
    graph.add(Collections.synchronizedList(new ArrayList<>()));
    graph.add(Collections.synchronizedList(new ArrayList<>()));
    graph.add(Collections.synchronizedList(new ArrayList<>()));
    graph.add(Collections.synchronizedList(new ArrayList<>()));
    graph.add(Collections.synchronizedList(new ArrayList<>()));

    add(0,1,3);
    add(0,3,6);
    add(2,4,7);
    add(1,2,1);
  }


  public void add(int x, int y, int z) {
    graph.get(x).add(new Pair<>(y, z));
    graph.get(y).add(new Pair<>(x, z));
  }

  public void remove(int x1, int y1) {
    Pair<Integer, Integer> pair = graph.get(x1).get(y1);
    graph.get(x1).remove(y1);
    graph.get(pair.first).remove(new Pair<>(x1, pair.second));
  }

  public int dfs(int s, int cost, int finish, boolean[] used) {
    used[s] = false;
    if (s == finish) {
      return cost;
    }
    for (Pair<Integer, Integer> pair : graph.get(s)) {
      if (used[pair.first])
      dfs(pair.first, cost + pair.second, finish, used);
    }
    return 0;
  }

  @Override
  public String toString() {
    StringBuilder s = new StringBuilder();
    for (int i = 0; i < graph.size(); i ++) {
      for (int j = 0; j < graph.get(i).size(); j ++) {
        Pair pair = graph.get(i).get(j);
        s.append(String.format("{%d, %d}", pair.first, pair.second));
      }
      s.append("\n");
    }
    return s.toString();
  }
}
