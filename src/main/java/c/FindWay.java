package c;

import lombok.SneakyThrows;

public class FindWay implements Runnable{

  static int a, b;

  @SneakyThrows
  @Override
  public void run() {
    while (true) {
      boolean f = false;

      for (Pair<Integer, Integer> pair : Main.map.graph.get(a)) {
        if (pair.first.equals(b)){
          System.out.println(pair.second);
          f = true;
          break;
        }
      }

      if (!f) {
        boolean[] used = new boolean[Main.map.graph.size()];
        for (int i = 0; i < Main.map.graph.size(); i ++) {
          used[i] = true;
        }
        System.out.println(Main.map.dfs(a, 0, b, used));
      }


      Thread.sleep(20000);
    }
  }
}
