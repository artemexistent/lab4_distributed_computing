package b;

import lombok.SneakyThrows;

import java.util.Random;

public class Nature implements Runnable {

  private Random random = new Random();

  @SneakyThrows
  @Override
  public void run() {
    while (true) {
      synchronized (Main.garden) {
        int x = random.nextInt(Main.garden.length);
        int y = random.nextInt(Main.garden[0].length);
        Main.garden[x][y] = random.nextInt(4) - 1;
        System.out.printf("%d %d = %d\n", x, y, Main.garden[x][y]);
      }
      Thread.sleep(10000);
    }
  }
}
