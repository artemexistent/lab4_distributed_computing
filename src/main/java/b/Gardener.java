package b;

import lombok.SneakyThrows;

public class Gardener implements Runnable{
  @SneakyThrows
  @Override
  public void run() {
    while (true) {
      synchronized (Main.garden) {
        boolean f = false;
        for (int i = 0; i < Main.garden.length && !f; i ++) {
          for (int j = 0; j < Main.garden[i].length && !f; j ++) {
            if (Main.garden[i][j] == -1) {
              Main.garden[i][j] = 1;
              f = true;
            }
          }
        }
      }
      Thread.sleep(5000);
    }
  }
}
