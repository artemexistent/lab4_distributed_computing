package b;

public class Main {

  public static int[][] garden = {
      {0, 1, 0, 2, -1, 0, 2},
      {0, 1, 0, 2, -1, 0, 2},
      {0, 1, 0, 2, -1, 0, 2},
      {0, 1, 0, 2, -1, 0, 2},
      {0, 1, 0, 2, -1, 0, 2},
      {0, 1, 0, 2, -1, 0, 2}
  };


  public static void main(String[] args) {
    new Thread(new Nature()).start();
    new Thread(new Monitor1()).start();
    new Thread(new Monitor2()).start();
    new Thread(new Gardener()).start();
  }

  public static String string() {
    StringBuilder s = new StringBuilder();
    for (int i = 0; i < garden.length; i ++) {
      for (int j = 0; j < garden[i].length; j ++) {
        s.append(garden[i][j]).append(" ");
      }
      s.append("\n");
    }
    return s.toString();
  }
}
