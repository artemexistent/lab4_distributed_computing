package b;

import lombok.SneakyThrows;

import java.util.concurrent.locks.ReentrantLock;

public class Monitor2 implements Runnable {

  public static ReentrantLock locker = new ReentrantLock();

  @SneakyThrows
  @Override
  public void run() {
    while (true) {
      synchronized (Main.garden) {
        locker.lock();
        System.out.println(Main.string());
        locker.unlock();
      }
      Thread.sleep(10000);
    }
  }
}
