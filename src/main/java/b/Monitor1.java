package b;

import lombok.SneakyThrows;

import java.io.File;
import java.io.FileWriter;
import java.util.concurrent.locks.ReentrantLock;

public class Monitor1 implements Runnable{

  private File file= new File("b.txt");
  public static ReentrantLock locker = new ReentrantLock();

  @SneakyThrows
  @Override
  public void run() {
    while (true) {
      synchronized (Main.garden) {
        locker.lock();
        FileWriter writer = new FileWriter(file, true);
        writer.write("\n\n\n" + Main.string());
        writer.close();
        locker.unlock();
      }
      Thread.sleep(10000);
    }
  }
}
