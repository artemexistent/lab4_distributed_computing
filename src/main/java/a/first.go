package main

import (
	"bufio"
	"container/list"
	"fmt"
	"os"
	"strings"
	"sync"
	"time"
)

var fileMutex sync.Mutex

func findPhone(secondName string) {
	fileMutex.Lock()
	file, err := os.Open("a.txt")
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)

	for scanner.Scan() {
		if strings.HasPrefix(scanner.Text(), secondName) {
			fmt.Println(scanner.Text())
		}
	}
	fileMutex.Unlock()
}
func findName(phone string) {
	fileMutex.Lock()
	file, err := os.Open("a.txt")
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	defer file.Close()
	scanner := bufio.NewScanner(file)

	for scanner.Scan() {
		if strings.HasSuffix(scanner.Text(), phone) {
			fmt.Println(scanner.Text())
		}
	}
	fileMutex.Unlock()
}
func deleteAndAdd(phone string, firstName string, secondName string, newPhone string) {
	fileMutex.Lock()
	file, err := os.Open("a.txt")
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	defer file.Close()
	scanner := bufio.NewScanner(file)
	l := list.New()
	for scanner.Scan() {
		s := scanner.Text()
		if !strings.HasSuffix(s, phone) {
			l.PushBack(s)
		}
	}
	os.Remove("a.txt")
	file, _ = os.Create("a.txt")

	w := bufio.NewWriter(file)
	for e := l.Front(); e != nil; e = e.Next() {
		str := fmt.Sprintf("%v", e.Value)
		w.WriteString(str + "\n")
	}
	w.WriteString(secondName + " " + firstName + " " + newPhone)
	w.Flush()
	file.Close()
	fileMutex.Unlock()
}

func main() {
	go findName("380905568172")
	go findPhone("Самойлич")
	go deleteAndAdd("380995868172", "СазоFов", "НіFіта", "380995028172")
	go findName("380995868172")
	go findPhone("СазоFов")
	time.Sleep(2 * time.Second)
}
